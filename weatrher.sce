//Agnieszka Sasiela, Technical Physics, group 1.2
//Matlab in physical applications, task 6
//This program is supposed to import the synoptic data for 3 Polish cities: Wrocław, Racibórz and Bielsko-biała...
//...from two websites: https://www.imgw.pl/, and wttr.in/.. in json format, then analyse  them and save in csv file.
// The data need to imported hourly and then printed on the screen.
//This program must have the access to the Internet to import the data.

while 1==1 //main loop 
path="C:\Users\agabi\Documents\weatherData.csv"; //ATTENTION ! Please change this path (Documents on your computer) to allow this program work (read csv data from the location it saves it firstly)
c=clock(); //In the beggining the time of the dowloadning must be precisely determined. I chose the 40th minute of every hour (e.g. 11:40 AM, 2:40 PM)
d=[int(c(5))];
t=[40.];
g=d==t; //check if the current minute value is equal to "40"

 if g== %T // if the minute value is equal to "40" the data are imported 
        for i=1:1
            data_RCB=getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12540');
            data_WRO=getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12424');
            data_BB=getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12600');
            f1=getURL('http://wttr.in/wroclaw?format=j1');
            f2=getURL('http://wttr.in/bielsko-biala?format=j1');
            f3=getURL('http://wttr.in/raciborz?format=j1');
        end

        uni_matrix_imgw=zeros(9,1); // these 4 matrixs will be used in the the following for loop to save:
        matrix_wro_imgw=zeros(9,1); //the date,time,temperature,pressure,windspeed and humidity for each of these 3 cities
        matrix_rcb_imgw=zeros(9,1); // I use one "universal " matrix in the loop but in the end I move the data to one matrix, each matrix for one city
        matrix_bb_imgw=zeros(9,1); // The matrix has 9 cells because the unused now will be needed later
        for i=1:3
            if i==1;
                uni_matrix_imgw=matrix_wro_imgw;
                imgw= mgetl("12424");
            end;
            if i==2;
                uni_matrix_imgw=matrix_rcb_imgw;
                imgw= mgetl("12540");
            end;
            if i==3;
                uni_matrix_imgw=matrix_bb_imgw;
                imgw= mgetl("12600");
            end;

            imgw=strsplit((imgw),",", 10);

            //dat, in this program the date might be a string(to look like  11:00)
            dat=strsplit(imgw(3), ":",2);
            dat_2=dat(2);
            dat_3=strsplit(dat_2,"",length(dat_2)-1);
		    dat_4=dat_3(2) + dat_3(3)+dat_3(4)+dat_3(5)+dat_3(6) + dat_3(7)+dat_3(8)+dat_3(9)+dat_3(10)+dat_3(11);

            //time I want ot have a real number so I must "take in from the json file" the final value is the tm_6
            tm=strsplit(imgw(4), ":",2);
            tm_2=tm(2);
            tm_3=strsplit(tm_2,"",length(tm_2)-1);
            if length(tm_2)==4.;
                tm_4=tm_3(2)+tm_3(3);
            end;
            if length(tm_2)==3.;
                tm_4=tm_3(2);
            end;
            tm_5=int(strtod(tm_4));
            tm_6=tm_4 + ":00";

            //temperature I want ot have a real number so I must "take in from the json file" the final value is the tem_5
            tem=strsplit(imgw(5), ":",2);
            tem_2=tem(2);
            tem_3=strsplit(tem_2,"",length(tem_2)-1);
            if length(tem_2)==5;
                tem_4=tem_3(2)+tem_3(3)+tem_3(4);
            end;
            if length(tem_2)==6;
                tem_4=tem_3(2)+tem_3(3)+tem_3(4)+tem_3(5);
            end;
            if length(tem_2)==7;
                tem_4=tem_3(2)+tem_3(3)+tem_3(4)+tem_3(5)+ tem_3(6);
            end;
            tem_5=int(strtod(tem_4));

            //pressure the final value is pre_5
		    pre=strsplit(imgw(10), ":",2);
		    pre_2=pre(2);
		    pre_3=strsplit(pre_2,"",length(pre_2)-1);
		    if length(pre_2)==9;
                  pre_4=pre_3(2)+pre_3(3)+pre_3(4)+pre_3(5);
		    end;
		    if length(pre_2)==8;
                  pre_4=pre_3(2)+pre_3(3)+pre_3(4);
		    end;
		    pre_5=int(strtod(pre_4));

		    //windspeed the final value is ve_5
		    ve=strsplit(imgw(6), ":",2);
		    ve_2=ve(2);
		    ve_3=strsplit(ve_2,"",length(ve_2)-1);
		    if length(ve_2)==3;
                 ve_4=ve_3(2);
		    end;
		    if length(ve_2)==4;
                 ve_4=ve_3(2)+ve_3(3);
		    end;
		    if length(ve_2)==5;
                 ve_4=ve_3(2)+ve_3(3) + ve_3(4);
		    end;
		    ve_5=int(strtod(ve_4));

		    //humidity the final value saved to matrix is hu_5
		    hu=strsplit(imgw(8), ":",2);
		    hu_2=hu(2);
		    hu_3=strsplit(hu_2,"",length(hu_2)-1);
		    if length(hu_2)==6;
                  hu_4=hu_3(2)+hu_3(3)+hu_3(4)+hu_3(5);
		    end;
		    if length(hu_2)==7;
                  hu_4=hu_3(2)+hu_3(3)+hu_3(4)+hu_3(5)+hu_3(6);
		    end;
		    hu_5=int(strtod(hu_4));

		    // computing the sensible temperature 
		    //the method in this part of code was taken from https://gist.github.com/jfcarr/e68593c92c878257550d?fbclid=IwAR1_Nl7gfi30OJGpMW8DVooTiVV8Nv_xX3uS6a5jUhEJB3sS2QS9mab6vFE
		    //vTemperature =matrix_wro_imgw(2)
		    //vWindSpeed = matrix_wro_imgw(4)
		    //vRelativeHumidity =matrix_wro_imgw(5)
		    if tem_5 <= 50 & ve_5 >= 3
                  sete = 35.74 + (0.6215*tem_5) - 35.75*(ve_5**0.16) + ((0.4275*tem_5)*(ve_5**0.16));
		    else
                  sete = tem_5;
		    end


		   uni_matrix_imgw(3,1)=tm_5; //saving the values- real numbers in the universal matrix
		   uni_matrix_imgw(4,1)=tem_5;
		   uni_matrix_imgw(5,1)=sete;
		   uni_matrix_imgw(6,1)=pre_5;
		   uni_matrix_imgw(7,1)=ve_5;
		   uni_matrix_imgw(8,1)=hu_5;
		   if i==1;
              matrix_wro_imgw=uni_matrix_imgw; //saving the data to matrix, each city has its own matrix
		   end;
		   if i==2;
              matrix_rcb_imgw=uni_matrix_imgw;
		   end;
		   if i==3;
              matrix_bb_imgw=uni_matrix_imgw;
		   end;
        end 
	

	    uni_matrix_wttr=zeros(9,1); //analogously  a universal matrix, and 3 matrixs to save the data for each city
	    matrix_wro_wttr=zeros(9,1);
	    matrix_rcb_wttr=zeros(9,1);
	    matrix_bb_wttr=zeros(9,1);
	    for i=1:3;

	
		  if i==1;
		  uni_matrix_wttr=matrix_wro_wttr;
		  wttr= mgetl("wroclaw");
		  end;
		  if i==2;
		  uni_matrix_wttr=matrix_rcb_wttr;
		  wttr= mgetl("raciborz");
		  end;
		  if i==3;
		  uni_matrix_wttr=matrix_bb_wttr;
		  wttr= mgetl("bielsko-biala");
		  end;

	      //date
	      date=wttr(50);
	      date_2=strsplit(date,":", 2);
	      date_3=date_2(2);
	      date_4=strsplit(date_3);
	      date_5=date_4(3)+date_4(4)+date_4(5)+date_4(6)+date_4(7)+date_4(8)+date_4(9)+date_4(10)+date_4(11)+date_4(12);


	     // wsadzanie czasu do macierzy 
	     t=wttr(8);
	     time=strsplit(t(1),":", 5);
	     time_2=time(2) + time(3);
	     time_3=strsplit(time_2,6);
	     time_4=time_3(1,1);
	     ampm=time_3(2,1);
	     time_5=strsplit(time_4,"", 5);
	     ampm_2=strsplit(ampm,"", 5);
	     ampm_3=ampm_2(2) + ampm_2(3);
	     time_6=time_5(3) + time_5(4)+ time_5(5) + time_5(6);
	     time_7=strtod(time_6);



	     am="AM"; // in this part of program I change the hour of measuremnt from the twelve-hour system to 24-hour system ( to see the difference between 2AM and 2PM)
	     pm="PM";
	     ampm_3=ampm_2(2) + ampm_2(3);
	     result=(ampm_3==am);
	     if result == %F;
   	        time_7=time_7+1200.;
	     end;

	     if i==1;
	       time_8=string(time_7);
           time_9=strsplit(time_8);
           time_10=time_9(1)+time_9(2)+":"+time_9(3)+time_9(4);
	     end;
	     if i==2;
	       time_11=string(time_7);
           time_12=strsplit(time_8);
           time_13=time_12(1)+time_12(2)+":"+time_12(3)+time_12(4);
	     end;
	     if i==3;
	       time_14=string(time_7);
           time_15=strsplit(time_8);
           time_16=time_15(1)+time_15(2)+":"+time_15(3)+time_15(4);
	     end;
	

	     //temperature
	     te=wttr(11);
	     temp_2=strsplit(te,":", 1);
	     temp_3=temp_2(2);
	     temp_4=strsplit(temp_3,"", length(temp_3));
	     if length(temp_3)==6.;
	       temp_5=strtod(temp_4(3));
	     end;
	     if length(temp_3)==7.;
	       temp_5=strtod(temp_4(3)+temp_4(4));
	     end;
	     if length(temp_3)==8.;
	       temp_5=strtod(temp_4(3)+temp_4(4)+temp_4(5));
	     end;

	       // feels like 
	     ste=wttr(4);
	     ste_2=strsplit(ste,":", 1);
	     ste_3=ste_2(2);
	     ste_4=strsplit(ste_3,"", length(ste_3));
	     if length(ste_3)==6.;
	         ste_5=strtod(ste_4(3));
	     end;
	     if length(ste_3)==7.;
	         ste_5=strtod(ste_4(3)+ste_4(4));
	
	     end;

	     //wsadzanie pressure do macierzy 
	     p=wttr(10);
	     p_2=strsplit(p,":", 1);
	     p_3=p_2(2);;
	     p_4=strsplit(p_3,"", length(p_3)-1);
	     if length(p_3)==9.;
	       p_5=p_4(3) + p_4(4) + p_4(5) + p_4(6);
	     end;
	     if length(p_3)==8.;
	       p_5=p_4(3) + p_4(4) + p_4(5);
	     end;
	     p_6=strtod(p_5);
	

	     //wind speed
	     v=wttr(28);
	     v_2=strsplit(v,":", 1);
	     v_3=v_2(2);
	     v_4=strsplit(v_3,"", length(v_3)-1);
	     if length(v_3)==6.;
	       v_5=v_4(3);
	       v_5;
	     end;
	     if length(v_3)==7.;
	       v_5=v_4(3)+v_4(4) ;
	       v_5;
	     end;
	     if length(v_3)==8.;
	       v_5=v_4(3)+v_4(4) +v_4(5) ;
	       v_5;
	     end;
	       v_6=strtod(v_5);
	

	     //humidity 

	     h=wttr(7);
	     h_2=strsplit(h,":", 1);
	     h_3=h_2(2);
	     h_4=strsplit(h_3,"", length(h_3)-1);
	     if length(h_3)==7.;
	       h_5=h_4(3)+h_4(4);
	     end;
	     if length(h_3)==8.;
	       h_5=h_4(3)+h_4(4)+h_4(5);
	     end;
	     h_6=strtod(h_5);

	     uni_matrix_wttr(3,1)=time_7;
	     uni_matrix_wttr(4,1)=temp_5;
	     uni_matrix_wttr(5,1)=ste_5;
	     uni_matrix_wttr(6,1)=p_6;
	     uni_matrix_wttr(7,1)=v_6;
	     uni_matrix_wttr(8,1)=h_6;

	     if i==1;
	       matrix_wro_wttr=uni_matrix_wttr;
	     end;
	     if i==2;
	       matrix_rcb_wttr=uni_matrix_wttr;
	     end;
	     if i==3;
	       matrix_bb_wttr=uni_matrix_wttr;
	     end;

	    end
	    matrix_wro_imgw;
	    matrix_rcb_imgw;
	    matrix_bb_imgw;

	    W=matrix_wro_imgw'; //transpose matrixs will be needed to save the data in csv
	    R=matrix_rcb_imgw';
	    B=matrix_bb_imgw';

	    matrix_wro_wttr;
	    matrix_rcb_wttr;
	    matrix_bb_wttr;

	   WR=matrix_wro_wttr';
	   RC=matrix_rcb_wttr';
	   BB=matrix_bb_wttr';

	    if isfile("weatherData.csv")==%F; //creating csv file, with proper headers and units 
	     csv_file=mopen("weatherData.csv", "a+");
	     headers=["Measurement_date","City","Measurement_hour","Temperature","Sensible_temperature","Pressure","Wind_speed","Humidity","Source_of_information"];
	     units=[" "," "," ","degrees Celsius","degrees Celsius","hPa","km/h","%"," "];
	     mfprintf(csv_file,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n',headers(1),',',headers(2),',',headers(3),',',headers(4),',',headers(5),',',headers(6),',',headers(7),',',headers(8),',',headers(9));
	     mfprintf(csv_file,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n',units(1),',',units(2),',',units(3),',',units(4),',',units(5),',',units(6),',',units(7),',',units(8),',',units(9));
	     mclose(csv_file);
	    end;

	    if isfile("weatherData.csv")==%T; //appending the data
	   	 csv_file=mopen("weatherData.csv", "a+");
    	 mfprintf(csv_file,"%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s%s\n",dat_4,",","Wroclaw",",",tm_6,",",W(4),",",W(5),",",W(6),",",W(7),",",W(8),",","IMGW");
	     mfprintf(csv_file,"%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s%s\n",dat_4,",","Raciborz",",",tm_6,",",R(4),",",R(5),",",R(6),",",R(7),",",R(8),",","IMGW");
	     mfprintf(csv_file,"%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s%s\n",dat_4,",","Bielsko-Biala",",",tm_6,",",B(4),",",B(5),",",B(6),",",B(7),",",B(8),",","IMGW");
	     mfprintf(csv_file,"%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s%s\n",date_5,",","Wroclaw",",",string(time_10),",",WR(4),",",WR(5),",",WR(6),",",WR(7),",",WR(8),",","WTTR");
	     mfprintf(csv_file,"%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s%s\n",date_5,",","Raciborz",",",string(time_13),",",RC(4),",",RC(5),",",RC(6),",",RC(7),",",RC(8),",","WTTR");
	     mfprintf(csv_file,"%s%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s%s\n",date_5,",","Bielsko-Biala",",",string(time_16),",",BB(4),",",BB(5),",",BB(6),",",BB(7),",",BB(8),",","WTTR");
         mclose(csv_file)
	    end;

       
        result= csvRead(path,ascii(44), [], 'string'); //printing the current maximum values
        MAX_T=max(strtod(result(:,4)));
        MAX_ST=max(strtod(result(:,5)));
        PRESSURE=max(strtod(result(:,6)));
        HUMIDITY=max(strtod(result(:,8)));
        msprintf("The maximum temperature so far is: %f \n", MAX_T)
        msprintf("The maximum sensible temperature so far is: %f \n", MAX_ST)
        msprintf("The maximum pressure so far is: %f \n", PRESSURE)
        msprintf("The maximum humidity so far is: %f \n", HUMIDITY)
 end
end
